import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BackStageItemUpdaterTest {

    private ItemUpdater itemUpdater;


    @Before
    public void before() {
        itemUpdater = new BackStageItemUpdater();
    }

    /*
    "Backstage passes", like aged brie,
    increases in Quality as it's SellIn value approaches; Quality
    increases by 2 when there are 10 days or less and
     by 3 when there are 5 days or less but Quality drops to 0 after the concert
     */
    @Test
    public void backstage_increase_when_sellIn_value_approaches() {
        Item backstage15 = new Item("Backstage passes to a TAFKAL80ETC concert", 15, 10);
        Item backstage9 = new Item("Backstage passes to a TAFKAL80ETC concert", 9, 10);
        Item backstage4 = new Item("Backstage passes to a TAFKAL80ETC concert", 4, 10);

        itemUpdater.updateItem(backstage15);
        itemUpdater.updateItem(backstage9);
        itemUpdater.updateItem(backstage4);

        assertEquals(11, backstage15.getQuality());
        assertEquals(12, backstage9.getQuality());
        assertEquals(13, backstage4.getQuality());
    }

    // Backstage Quality drops to 0 after the concert
    @Test
    public void backstage_quality_should_be_0_after_concert() {
        Item backstage0 = new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10);
        Item backstage1 = new Item("Backstage passes to a TAFKAL80ETC concert", -1, 10);

        itemUpdater.updateItem(backstage0);
        itemUpdater.updateItem(backstage1);

        assertEquals(0, backstage0.getQuality());
        assertEquals(0, backstage1.getQuality());
    }
}