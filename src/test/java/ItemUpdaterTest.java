import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ItemUpdaterTest {

    private ItemUpdater itemUpdater;


    @Before
    public  void before() {
        itemUpdater = new ItemUpdater();
    }

    @Test
    public void normal_Item_decrease_one_in_quality_and_one_in_sellin(){
        Item elixir = new Item("Elixir of the Mongoose", 5, 7);

        itemUpdater.updateItem(elixir);

        assertEquals(6, elixir.getQuality());
        assertEquals(4, elixir.getSellIn());
    }

    @Test
    public void normal_Item_decrease_two_in_quality_and_one_in_sellin(){
        Item elixir = new Item("Elixir of the Mongoose", -1, 7);

        itemUpdater.updateItem(elixir);

        assertEquals(5, elixir.getQuality());
        assertEquals(-2, elixir.getSellIn());
    }

    //The Quality of an item is never negative
    @Test
    public void items_decrease_two_in_quality_and_one_in_sellin(){
        Item dexterity = new Item("+5 Dexterity Vest", 10, 0);
        Item dexterity1 = new Item("+5 Dexterity Vest", -1, 0);

        itemUpdater.updateItem(dexterity);
        itemUpdater.updateItem(dexterity1);

        assertTrue(dexterity.getQuality() >= 0);
        assertTrue(dexterity1.getQuality() >= 0);
    }


}