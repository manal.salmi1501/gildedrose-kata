import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SulfurasItemUpdaterTest {
    private ItemUpdater itemUpdater;


    @Before
    public  void before() {
        itemUpdater = new SulfurasItemUpdater();
    }

    @Test
    public void sulfuras_item_should_not_be_modified(){
        Item sulfuras = new Item("Sulfuras", 0, 80);

        itemUpdater.updateItem(sulfuras);

        assertEquals(80, sulfuras.getQuality());
        assertEquals(0, sulfuras.getSellIn());
    }

}