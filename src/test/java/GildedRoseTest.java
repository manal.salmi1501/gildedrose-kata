import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;


public class GildedRoseTest {

	/*

	    items = new ArrayList<Item>();
        items.add(new Item("+5 Dexterity Vest", 10, 20));
        items.add(new Item("Aged Brie", 2, 0));
        items.add(new Item("Elixir of the Mongoose", 5, 7));
        items.add(new Item("Sulfuras, Hand of Ragnaros", 0, 80));
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20));
        items.add(new Item("Conjured Mana Cake", 3, 6));
	 */

	ArrayList<Item> items;

	@Before
	public void before(){
		this.items = new ArrayList<Item>();
	}

	@Test
	public void sulfuras_never_decrease_in_quality(){
		Item sulfuras = new Item("Sulfuras, Hand of Ragnaros", 0, 80);
		items.add(sulfuras);

		GildedRose.updateQuality(items);

		assertEquals(80, sulfuras.getQuality());
		assertEquals(0, sulfuras.getSellIn());
	}

	// ! Once the sell by date has passed, Quality degrades twice as fast
	@Test
	public void normal_Item_decrease_one_in_quality_and_one_in_sellin(){
		Item elixir = new Item("Elixir of the Mongoose", 5, 7);
		items.add(elixir);
		Item elixir0 = new Item("Elixir of the Mongoose", 0, 7);
		items.add(elixir0);

		GildedRose.updateQuality(items);

		assertEquals(6, elixir.getQuality());
		assertEquals(4, elixir.getSellIn());
		assertEquals(6, elixir0.getQuality());
	}

	//Once the sell by date has passed, Quality degrades twice as fast
	@Test
	public void normal_Item_decrease_two_in_quality_and_one_in_sellin(){
		Item elixir = new Item("Elixir of the Mongoose", -1, 7);
		items.add(elixir);

		GildedRose.updateQuality(items);

		assertEquals(5, elixir.getQuality());
		assertEquals(-2, elixir.getSellIn());
	}

	//The Quality of an item is never negative
	@Test
	public void quality_is_never_negative(){
		Item dexterity = new Item("+5 Dexterity Vest", 10, 0);
		items.add(dexterity);
		Item aged = new Item("Aged Brie", 2, 0);
		items.add(aged);
		Item backstage = new Item("Backstage passes to a TAFKAL80ETC concert", 15, 0);
		items.add(backstage);

		GildedRose.updateQuality(items);

		assertTrue(dexterity.getQuality() >= 0);
		assertTrue(aged.getQuality() >= 0);
		assertTrue(backstage.getQuality() >= 0);
	}


	// "Aged Brie" actually increases in Quality the older it gets
	@Test
	public void aged_Brie_Item_increase_in_quality(){
		Item aged = new Item("Aged Brie", 2, 5);
		items.add(aged);

		GildedRose.updateQuality(items);

		assertEquals(6, aged.getQuality());
		assertEquals(1, aged.getSellIn());
	}

	// "Aged Brie" actually increases in Quality the older it gets
	@Test
	public void aged_Brie_Item_increase_twice_in_quality_when_sellIn_is_smaller_than_0(){
		Item aged = new Item("Aged Brie", -1, 5);
		items.add(aged);

		GildedRose.updateQuality(items);

		assertEquals(7, aged.getQuality());
		assertEquals(-2, aged.getSellIn());
	}

	// The Quality of an item is never more than 50
	@Test
	public void quality_is_never_more_50(){
		Item dexterity = new Item("+5 Dexterity Vest", 10, 50);
		items.add(dexterity);
		Item aged_brie = new Item("Aged Brie", 2, 50);
		items.add(aged_brie);
		Item backstage = new Item("Backstage passes to a TAFKAL80ETC concert", 15, 50);
		items.add(backstage);

		GildedRose.updateQuality(items);

		assertTrue(dexterity.getQuality() <= 50);
		assertTrue(aged_brie.getQuality() <= 50);
		assertTrue(backstage.getQuality() <= 50);
	}

	/*
	"Backstage passes", like aged brie,
	increases in Quality as it's SellIn value approaches; Quality
	increases by 2 when there are 10 days or less and
	 by 3 when there are 5 days or less but Quality drops to 0 after the concert
	 */
	@Test
	public void backstage_increase_when_sellIn_value_approaches() {
		Item backstage15 = new Item("Backstage passes to a TAFKAL80ETC concert", 15, 10);
		items.add(backstage15);
		Item backstage9 = new Item("Backstage passes to a TAFKAL80ETC concert", 9, 10);
		items.add(backstage9);
		Item backstage4 = new Item("Backstage passes to a TAFKAL80ETC concert", 4, 10);
		items.add(backstage4);

		GildedRose.updateQuality(items);

		assertEquals(11, backstage15.getQuality());
		assertEquals(12, backstage9.getQuality());
		assertEquals(13, backstage4.getQuality());
	}

	// Backstage Quality drops to 0 after the concert
	@Test
	public void backstage_quality_should_be_0_after_concert(){
		Item backstage0 = new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10);
		items.add(backstage0);
		Item backstage1 = new Item("Backstage passes to a TAFKAL80ETC concert", -1, 10);
		items.add(backstage1);

		GildedRose.updateQuality(items);

		assertEquals(0, backstage0.getQuality());
		assertEquals(0, backstage1.getQuality());
	}
}
