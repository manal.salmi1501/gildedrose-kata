import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AgedBrieitemUpdaterTest {
    private ItemUpdater itemUpdater;


    @Before
    public void before() {
        itemUpdater = new AgedBrieitemUpdater();
    }

    // "Aged Brie" actually increases in Quality the older it gets
    @Test
    public void aged_Brie_Item_increase_in_quality(){
        Item aged = new Item("Aged Brie", 2, 5);

        itemUpdater.updateItem(aged);

        assertEquals(6, aged.getQuality());
        assertEquals(1, aged.getSellIn());
    }

    // "Aged Brie" actually increases in Quality the older it gets
    @Test
    public void aged_Brie_Item_increase_twice_in_quality_when_sellIn_is_smaller_than_0(){
        Item aged = new Item("Aged Brie", -1, 5);

        itemUpdater.updateItem(aged);

        assertEquals(7, aged.getQuality());
        assertEquals(-2, aged.getSellIn());
    }

}