public class BackStageItemUpdater extends ItemUpdater {

    @Override
    public void updateItem(Item item) {
        if(item.getSellIn() > 0){
            if (item.getQuality() < 50) {
                item.setQuality(item.getQuality() + 1);

                if (item.getSellIn() < 11) {
                    if (item.getQuality() < 50) {
                        item.setQuality(item.getQuality() + 1);
                    }
                }

                if (item.getSellIn() < 6) {
                    if (item.getQuality() < 50) {
                        item.setQuality(item.getQuality() + 1);
                    }
                }
            }
        } else {
            item.setQuality(0);
        }

        item.sellIn--;
    }


}
