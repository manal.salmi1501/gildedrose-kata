public class ItemUpdater {

    public void updateItem(Item item){
        decreaseQualityIfPossible(item);

        if(item.getSellIn() < 0){
            decreaseQualityIfPossible(item);
        }

        item.sellIn--;
    }

    private void decreaseQualityIfPossible(Item item){
        if(item.getQuality() > 0){
            item.setQuality(item.getQuality()-1);
        }
    }
}
