public class AgedBrieitemUpdater extends ItemUpdater {

    @Override
    public void updateItem(Item item) {
        if (item.getQuality() < 50)
        {
            item.setQuality(item.getQuality() + 1);
        }

        if(item.getSellIn() < 0){
            if (item.getQuality() < 50)
            {
                item.setQuality(item.getQuality() + 1);
            }
        }
        item.sellIn--;
    }


}
