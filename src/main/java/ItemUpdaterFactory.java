public class ItemUpdaterFactory {

    public static ItemUpdater createItemUpdater(Item item) {
        if(item.getName().contains("Backstage")){
            return new BackStageItemUpdater();
        }
        if(item.getName().contains("Sulfuras")) {
            return new SulfurasItemUpdater();
        }
        if(item.getName().contains("Aged Brie")){
            return new AgedBrieitemUpdater();
        }
        return new ItemUpdater();
    }
}
